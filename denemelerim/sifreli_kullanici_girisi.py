###########################################
# Author:Dessasin                         #
# E-mail:muhammednurullahtorun@gmail.com  #
# github.com/dessasin                     #
# gitlab.com/dessasin                     #
# Hakkında:                               #
###########################################

users={"root":"toor","dessasin":"dessas"}
sec_ansver={"root":"sudo","dessasin":"sagopa"}

kadi=""
pasw=""
menu="""
[1]Login
[2]Register
[3]Reset Password
[Q]Quit
"""
#----------------Register-----------------------#
def add_user():# bu kısımda kullanıcı ekleme fonksiyonunu tanımladık
        kadi=input("Lütfen kullanıcı adını giriniz: ") # kullanıcı adını aldık
       
        if kadi in users.keys(): # eğer kullanıcı adı eski kullanıcı adları ile aynı ise tekrar seçtirdik bu kısımda recursion kullandım
            print("Kullanıcı adı mevcut farklı bir kullanıcı adı seçiniz")
            add_user()# recursion yani fonksiyon tekrar çağırdım
        else:
             pasw=input("Lütfen parolanızı  giriniz: ")# şifreyi aldık
             secans=input("En sevdiğiniz şarkıcının adı: ")# kullanıcının güvenlik sorusunun cevabını aldık
             users[kadi]=pasw# kullanıcı adı ile şifreyi users sözlüğüne ekledik
             sec_ansver[kadi]=secans# kullanıcı adı ve güvenlik sorusu cevabını sözlüğe ekledik
            
            
#--------------------Login--------------------#
def login_func(x,y):# login fonksiyonunu tanımladık
    if x in users:# kullanıcı adının olup olmadığını kontrol ettik eer varsa devam ettik 
        if y ==users[x]: # kullanıcı adı ile parola değerini karşılaştırdık
            print("Giriş Başarılı Hoş Geldin {}".format(x))
        elif y != users[x]:# eğer kullanıcı adı ile parola aynı değilse olacakları listeledik
            sifrst=input("Parola hatalı tekrar deneyiniz,parolanızı sıfırlamak ister misiniz ? e/h: ")    
            if sifrst =="e"or sifrst=="E":# parola sıfırlama konusunda bilgi verdik istek dahilinde parola sıfırlamaya yönlendirdik
                rest_pasw(x)# "x" kullanıcı adını içeriyor 
    else:
        input("Kullanıcı adı hatalı tekrar deneyiniz")
        
#-----------Reset Password-------------------#
def rest_pasw(username):# parola sıfırlama işlemini tanımladık
        if username in sec_ansver:
            print("===Parola sıfırlama işlemi için lütfen {} kullanıcısının güvenlik sorusunu yanıtlayınız===".format(username))
            yanit=input("{}'ın en sevdiği şarkıcı kimdir?: ".format(username))#güvenlik sorusu yanıtını aldık
            if yanit == sec_ansver[username]:# kullanıcının sec_ansver sözlüğündeki cevabı ile şu anki cevabını karşılaştırdık
                print("Güvenlik sorusu doğru yanıtlandı")
                new_pasw= input("Yeni parolanızı giriniz: ")#yeni parolayı aldık
                users[username]=new_pasw# users içindeki parolayı değiştirdik
                print("Parola değiştirme işlemi başarılı")
                input("Ana Menu İçin 'enter' basınız")
        else:
            input("Kullanıcı bulunamadı tekrar giriş yapınız")

#---------------Ana Menu------------------#
while True:
    print(menu)
    islem=input("Yapmak istediğiniz işlemi giriniz")
    
    if islem=="1":
        kadi=input("Lütfen kullanıcı adını giriniz: ")
        pasw=input("Lütfen parolanızı  giriniz: ")
        login_func(kadi,pasw)

    elif islem=="2":
        add_user()

    elif islem=="3":
        user=input("Lütfen parola sıfırlamak istediğiniz kullanıcıyı giriniz:")
        rest_pasw(user)

    elif islem=="q" or islem=="Q":
        quit()
    else:
        print("Lütfen geçerli bir işlem seçiniz")   